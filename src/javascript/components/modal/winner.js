import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import  App from '../../app';

export function showWinnerModal(fighter) {
	// call showModal function 
	const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
	bodyElement.innerHTML = `Winner is ${fighter.name}` + '<br><img src="' + fighter.source + '" alt="' + fighter.name + '">';
	showModal( {
		title: 'Game over', 
		bodyElement, 
		onClose: () => {
			document.getElementById('root').innerHTML = '';
			new App();
		} 
	} );
}
